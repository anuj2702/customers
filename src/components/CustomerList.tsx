import { Trans } from '@lingui/macro'
import { ListItemAvatar, ListItemText } from '@material-ui/core'
import { Date } from '@saastack/components/i18n'
import { ListContainerProps } from '@saastack/layouts'
import { ListContainer } from '@saastack/layouts/containers'
import React from 'react'
import { createFragmentContainer, graphql } from 'react-relay'
import { CustomerList_customers } from '../__generated__/CustomerList_customers.graphql'
import CustomerAvatar from './CustomerAvatar'

interface Props extends Omit<ListContainerProps<CustomerList_customers>, 'items'> {
    customers: CustomerList_customers
    companyAdmin: boolean
}

const CustomerList: React.FC<Props> = ({ customers, companyAdmin, ...props }) => {
    const [visibleColumns, onVisibleColumnsChange] = React.useState([
        'avatar',
        'name',
        'email',
        'telephones',
        'createdOn',
    ])

    const listProps = {
        render: (e: CustomerList_customers[0]) => [
            <ListItemAvatar key="avatar">
                <CustomerAvatar companyAdmin={companyAdmin} customer={e} />
            </ListItemAvatar>,
            <ListItemText
                key="name"
                primary={
                    <>
                        {e.firstName} {e.lastName}
                    </>
                }
                secondary={e.email}
            />,
        ],
    }
    const tableProps = {
        config: [
            {
                key: 'avatar',
                filterLabel: <Trans>Image</Trans>,
            },
            {
                key: 'name',
                label: <Trans>Name</Trans>,
            },
            {
                key: 'email',
                label: <Trans>Email</Trans>,
            },
            {
                key: 'telephones',
                label: <Trans>Phone</Trans>,
            },
            {
                key: 'createdOn',
                label: <Trans>Customer Since</Trans>,
            },
            // {
            //     key: 'lastBookedOn',
            //     label: <Trans>Last Activity</Trans>,
            // },
        ],
        render: {
            avatar: (e: CustomerList_customers[0]) => (
                <CustomerAvatar companyAdmin={companyAdmin} customer={e} />
            ),
            name: (e: CustomerList_customers[0]) => (
                <>
                    {e.firstName} {e.lastName}
                </>
            ),
            telephones: (e: CustomerList_customers[0]) => e.telephones.join(', '),
            createdOn: (e: CustomerList_customers[0]) => <Date>{e.createdOn as string}</Date>,
            // lastBookedOn: (e: CustomerList_customers[0]) => <Date>{e.lastBookedOn as string}</Date>,
        },
        visibleColumns,
        onVisibleColumnsChange,
        hoverCheckbox: true,
    }

    return (
        <ListContainer<CustomerList_customers>
            virtual
            listProps={listProps}
            tableProps={tableProps}
            items={customers}
            {...props}
            {...props}
        />
    )
}

export default createFragmentContainer(CustomerList, {
    customers: graphql`
        fragment CustomerList_customers on Customer @relay(plural: true) {
            id
            firstName
            lastName
            email
            telephones
            createdOn
            ...CustomerAvatar_customer
        }
    `,
})
