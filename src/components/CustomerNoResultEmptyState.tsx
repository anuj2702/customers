import { Trans } from '@lingui/macro'
import { EmptyStateContainer } from '@saastack/layouts/containers'
import React from 'react'

interface Props {
    onAction?: () => void
}

const CustomerNoResultEmptyState: React.FC<Props> = ({ onAction }) => {
    const props = {
        image: `${process.env.REACT_APP_SAASTACK_CDN_URL}/images/empty-states/booking-notification-empty-state.png`,
        actionLabel: <Trans>No results found</Trans>,
        message: <Trans>No results match the search criteria</Trans>,
    }
    return <EmptyStateContainer onAction={onAction} {...props} />
}

export default CustomerNoResultEmptyState
