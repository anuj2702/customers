import React from 'react'
import { CommonComponentProps as Props } from '@saastack/hooks/types'
import query from '../queries/GetCustomerQuery'
import { NotificationClickProps } from '@saastack/hooks/notifications'
import {
    GetCustomerQuery,
    GetCustomerQueryResponse,
} from '../__generated__/GetCustomerQuery.graphql'
import { Loading } from '@saastack/components'
import { Trans } from '@lingui/macro'
import { useAlert } from '@saastack/core'
import { fetchQuery } from 'relay-runtime'
import { useRelayEnvironment } from 'react-relay/hooks'
import { Link, useNavigate } from '@saastack/router'

export const CustomerCreated: React.FC<Props> = (props) => {
    return <></>
}

const CustomerViewAction: React.FC<NotificationClickProps> = ({ parent: id, ...props }) => {
    const [loading, setLoading] = React.useState(false)
    const environment = useRelayEnvironment()
    const showAlert = useAlert()
    const navigate = useNavigate()

    const fetchCustomer = () => {
        setLoading(true)
        fetchQuery<GetCustomerQuery>(environment, query, { id }, { force: true })
            .then((resp) => {
                if (resp.customer == null) {
                    setLoading(false)
                    showAlert(<Trans>Sorry, this customer has been deleted</Trans>, {
                        variant: 'info',
                    })
                    return
                }
                setLoading(false)
                navigate(`customers/${window.btoa(id!)}`)
            })
            .catch(() => {
                setLoading(false)
                showAlert(<Trans>Sorry, this customer does not exist</Trans>, {
                    variant: 'info',
                })
            })
    }

    React.useEffect(() => {
        fetchCustomer()
    }, [id])

    return <>{loading && <Loading />}</>
}

export const CustomerNotificationActions: React.FC<Props> = (props) => {
    return (
        <>
            <CustomerViewAction onClose={() => {}} {...props} />
        </>
    )
}
