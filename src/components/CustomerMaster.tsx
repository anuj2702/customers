import { Trans } from '@lingui/macro'
import loadable from '@loadable/component'
import { AddOutlined, CloudUploadOutlined } from '@material-ui/icons'
import { ActionItem, FilterItem, FilterItemValue, Filters } from '@saastack/components'
import { useAlias, useConfig } from '@saastack/core'
import { Roles, useCan } from '@saastack/core/roles'
import { Layout } from '@saastack/layouts'
import { PubSub } from '@saastack/pubsub'
import { Route, Routes, useNavigate, useRouteMatch } from '@saastack/router'
import { useDidMountEffect } from '@saastack/utils'
import { forEach, keys } from 'lodash-es'
import React from 'react'
import { createPaginationContainer, graphql, RelayPaginationProp } from 'react-relay'
import useForceUpdate from 'use-force-update'
import { CustomerMaster_customers } from '../__generated__/CustomerMaster_customers.graphql'
import { ReportFilterInput } from '../__generated__/CustomerPageQuery.graphql'
import namespace from '../namespace'
import CustomerList from './CustomerList'
import { Grid, MenuItem } from '@material-ui/core'
import { Select } from '@saastack/forms'
import moment from 'moment'

const CustomerAdd = loadable(() => import('./CustomerAdd'))
const CustomerDelete = loadable(() => import('./CustomerDelete'))
const CustomerDetail = loadable(() => import('./CustomerDetail'))
const CustomerEmptyState = loadable(() => import('./CustomerEmptyState'))
const CustomerNoResultEmptyState = loadable(() => import('./CustomerNoResultEmptyState'))
const CustomerUpdate = loadable(() => import('./CustomerUpdate'))

interface Props {
    companyAdmin: boolean
    locationAdmin: boolean
    parent: string
    customers: CustomerMaster_customers
    relay: RelayPaginationProp
}

const transformGender = (data: any) => {
    if (data.trim().toUpperCase() == 'MALE' || data.trim() == 'm' || data.trim() == 'M') {
        return 'MALE'
    }
    if (
        data.trim().toUpperCase() == 'FEMALE' ||
        data.trim() == 'female' ||
        data.trim() == 'f' ||
        data.trim() == 'F'
    ) {
        return 'FEMALE'
    }
    return 'UNSPECIFIED'
}

const options: FilterItem[] = [
    {
        key: 'firstName',
        label: <Trans>First Name</Trans>,
        type: 'input',
    },
    {
        key: 'lastName',
        label: <Trans>Last Name</Trans>,
        type: 'input',
    },
    {
        key: 'email',
        label: <Trans>Email</Trans>,
        type: 'input',
    },
    {
        key: 'phoneNumber',
        label: <Trans>Phone</Trans>,
        type: 'input',
    },
    {
        key: 'tags',
        label: <Trans>Tags</Trans>,
        type: 'input',
    },
    {
        key: 'customerCompanyId',
        label: <Trans>Company</Trans>,
        type: 'input',
    },
]

const sortMap: Record<string, { sortBy: string; direction: string }> = {
    ascName: {
        direction: 'Ascending',
        sortBy: 'FirstName',
    },
    descName: {
        direction: 'Descending',
        sortBy: 'FirstName',
    },
    ascEmail: {
        direction: 'Ascending',
        sortBy: 'Email',
    },
    descEmail: {
        direction: 'Descending',
        sortBy: 'Email',
    },
    ascCreatedOn: {
        direction: 'Ascending',
        sortBy: 'CreatedOn',
    },
    descCreatedOn: {
        direction: 'Descending',
        sortBy: 'CreatedOn',
    },
}
const sortLabelMap: Record<string, React.ReactNode> = {
    ascName: <Trans>Ascending by Name</Trans>,
    descName: <Trans>Descending by Name</Trans>,
    ascEmail: <Trans>Ascending by Email</Trans>,
    descEmail: <Trans>Descending by Email</Trans>,
    ascCreatedOn: <Trans>Ascending by Created on</Trans>,
    descCreatedOn: <Trans>Descending by Created on</Trans>,
}

const CustomerMaster: React.FC<Props> = ({
    companyAdmin,
    locationAdmin,
    customers: {
        customers: { edges: customerEdges },
    },
    parent,
    relay: { hasMore, isLoading, loadMore, refetchConnection },
}) => {
    const [filter, setFilter] = React.useState<FilterItemValue>({})
    const { locations: _locations, activeApps, companyId, locationId } = useConfig()
    const [tab, setTab] = React.useState<number>(0)
    const [bulkImportOpen, setBulkImportOpen] = React.useState<boolean>(false)
    const [selectCustomer, setSelectCustomer] = React.useState<string[]>([])
    const forceUpdate = useForceUpdate()
    const variables = { parent }
    const navigate = useNavigate()
    const alias = useAlias('Customers', { singular: 'Customer', plural: 'Customers' })
    const locationAlias = useAlias('Locations', { singular: 'Location', plural: 'Locations' })
    const heading = alias?.plural === 'Customers' ? <Trans>Customers</Trans> : alias?.plural
    const can = useCan()
    const canManage = (id: string) => can([Roles.CustomersEditor, Roles.CustomersAdmin], id)
    const managesCompanyOrLocation = canManage(parent) || canManage(locationId!)
    const match = useRouteMatch<{ id: string }>(':id')
    let selected = ''
    if (match?.params.id) {
        try {
            selected = window.atob(match?.params.id)
        } catch (e) {}
    }
    const locations = _locations?.map((e) => ({
        label: e.node?.name,
        value: e.node?.id,
    }))
    const [sortOrder, setSortOrder] = React.useState('ascCreatedOn')
    const actions: ActionItem[] = managesCompanyOrLocation
        ? [
              {
                  icon: CloudUploadOutlined,
                  onClick: () => setBulkImportOpen(true),
                  title: <Trans>Import</Trans>,
              },
              { icon: AddOutlined, onClick: () => navigate('add'), title: <Trans>Add</Trans> },
          ]
        : []
    const customers = customerEdges.map((s) => s.node!)
    const refetchCustomers = () => {
        let reportFilters: ReportFilterInput[] = []
        let customerFilters: Record<string, any> = {}
        forEach(filter, (value, key) => {
            const opt = options.find((o) => o.key === key)
            switch (opt?.type) {
                case 'date-range':
                    reportFilters = [
                        ...reportFilters,
                        {
                            endDate: value.endDate ?? moment().endOf('d').toISOString(),
                            startDate: value.startDate,
                            fieldName: key as any,
                            operator: value.type,
                        },
                    ]
                    break
                case 'input':
                case 'checkbox':
                    customerFilters = {
                        ...customerFilters,
                        [opt.key]: value,
                    }
                    break
            }
        })
        if (!companyAdmin && !customerFilters.locationIds) {
            customerFilters.locationIds = _locations?.map((e) => e.node?.id) ?? []
        }
        refetchConnection(20, forceUpdate, {
            parent,
            filters: reportFilters,
            ...customerFilters,
            tags: !!filter.tags ? [filter.tags] : null,
            ...sortMap[sortOrder],
        })
        forceUpdate()
    }
    useDidMountEffect(() => {
        refetchCustomers()
    }, [parent, filter, sortOrder])

    React.useEffect(() => {
        PubSub.publish(namespace.fetch, customers)
    }, [customers])

    const _loadMore = () => {
        if (!hasMore() || isLoading()) {
            return
        }
        loadMore(20, () => forceUpdate())
        forceUpdate()
    }

    const noCustomer = Boolean(
        !customers.length && !Object.keys(filter).length && !isLoading() && !hasMore()
    )
    const noSearchResult = Boolean(!customers.length && Object.keys(filter).length && !isLoading())

    const col1 = noCustomer ? (
        <CustomerEmptyState onAction={() => navigate('add')} />
    ) : noSearchResult ? (
        <CustomerNoResultEmptyState />
    ) : (
        <CustomerList
            companyAdmin={companyAdmin}
            highlighted={selected}
            hasMore={hasMore}
            isLoading={isLoading}
            loadMore={_loadMore}
            onItemClick={(id: string) => navigate(window.btoa(id || ''))}
            customers={customers}
            selectable
            selected={selectCustomer}
            onSelect={setSelectCustomer}
        />
    )

    const filterLabel = keys(filter).length ? (
        <Trans>Showing {alias?.plural.toLocaleLowerCase()} filter by</Trans>
    ) : (
        <Trans>Showing all active {alias?.plural.toLocaleLowerCase()}</Trans>
    )
    const filters = !noCustomer && (
        <Grid container alignItems="center">
            <Grid item xs={12} sm={6} md={8} lg={10} xl={11}>
                <Filters
                    value={filter}
                    onChange={setFilter}
                    options={options}
                    label={filterLabel}
                />
            </Grid>
            <Grid item xs={12} sm={6} md={4} lg={2} xl={1}>
                <Select
                    label={<Trans>Sort by</Trans>}
                    value={sortOrder}
                    variant="outlined"
                    onChange={(e) => setSortOrder(e.target.value)}
                >
                    {Object.keys(sortLabelMap).map((key) => (
                        <MenuItem value={key} key={key}>
                            {sortLabelMap[key]}
                        </MenuItem>
                    ))}
                </Select>
            </Grid>
        </Grid>
    )

    return (
        <Layout filters={filters} actions={actions} header={heading} col1={col1}>
            <Routes>
                <Route
                    path="add"
                    element={
                        <CustomerAdd
                            variables={variables}
                            refetch={refetchCustomers}
                            locationAdmin={locationAdmin}
                            companyAdmin={companyAdmin}
                        />
                    }
                />
                <Route
                    path=":id"
                    element={
                        <CustomerDetail
                            companyAdmin={companyAdmin}
                            tab={tab}
                            onTabChange={setTab}
                            customers={customers}
                            locationAdmin={locationAdmin}
                        />
                    }
                >
                    <Route
                        path="update"
                        element={
                            <CustomerUpdate companyAdmin={companyAdmin} customers={customers} />
                        }
                    />
                    <Route
                        path="delete"
                        element={
                            <CustomerDelete refetch={refetchCustomers} variables={variables} />
                        }
                    />
                </Route>
            </Routes>
        </Layout>
    )
}

export default createPaginationContainer(
    CustomerMaster,
    {
        customers: graphql`
            fragment CustomerMaster_customers on Query
            @argumentDefinitions(

            ) {

            }
        `,
    },
    {
        //todo: add config
    }
)
