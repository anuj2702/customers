import { PubSub } from '@saastack/pubsub'
import { Trans } from '@lingui/macro'
import { tail } from 'lodash-es'
import React from 'react'
import { Variables } from 'relay-runtime'
import { CustomerInput } from '../__generated__/CreateCustomerMutation.graphql'
import CustomerAddFormComponent from '../forms/CustomerAddFormComponent'
import CreateCustomerMutation from '../mutations/CreateCustomerMutation'
import namespace from '../namespace'
import CustomerAddInitialValues from '../utils/CustomerAddInitialValues'
import CustomerAddValidations from '../utils/CustomerAddValidations'
import { useAlert, useAlias, useConfig } from '@saastack/core'
import { FormContainerProps } from '@saastack/layouts'
import { FormContainer } from '@saastack/layouts/containers'
import { useRelayEnvironment } from 'react-relay/hooks'
import { useNavigate } from '@saastack/router'

interface Props extends Omit<FormContainerProps, 'formId'> {
    variables: Variables
    refetch?: () => void
    onAdd?: (response: CustomerInput) => void
    input?: string
    locationAdmin?: boolean
    companyAdmin?: boolean
}

const formId = 'customer-add-form'

interface CustomerAddInput extends CustomerInput {
    sendEmail: boolean
    sendSms: boolean
    name: string
}

const CustomerAdd: React.FC<Props> = ({
    variables,
    refetch,
    onExited,
    input = '',
    locationAdmin,
    companyAdmin,
    ...props
}) => {
    const alias = useAlias('Customers', { singular: 'Customer', plural: 'Customers' })
    const { locations, locationId, companyId, timezone } = useConfig()
    const environment = useRelayEnvironment()
    const navigate = useNavigate()
    const [open, setOpen] = React.useState(true)

    const navigateBack = () => {
        if (onExited) {
            return onExited()
        }
        navigate('../')
    }
    const handleClose = () => setOpen(false)

    const handleSubmit = (values: CustomerAddInput) => {
        setLoading(true)
        const nameParts = ((values as any).name || '').trim().split(' ')
        const customer = {
            ...values,
            firstName: nameParts[0],
            lastName: tail(nameParts).join(' '),
            email: values.email?.toLowerCase(),
        }
        //@ts-ignore
        const locationIds = values?.locationIds?.filter(Boolean) ?? []

        const parent = !!(values as any).locationId
            ? (values as any).locationId
            : (!companyAdmin && locationAdmin) || locations!.length === 1
            ? locationId
            : companyId
        const vars = {
            ...variables,
            parent,
        }
        CreateCustomerMutation.commit(
            environment,
            vars,
            customer,

            {
                email: values.sendEmail,
                sms: values.sendSms,
            },
            locationIds,
            { onSuccess, onError }
        )
    }

    const showAlert = useAlert()
    const [loading, setLoading] = React.useState(false)

    const onError = (e: string) => {
        setLoading(false)
        showAlert(e, {
            variant: 'error',
        })
    }

    const onSuccess = (response: CustomerInput) => {
        refetch?.()
        PubSub.publish(namespace.create, response)
        setLoading(false)
        showAlert(<Trans>{alias?.singular} created successfully!</Trans>, {
            variant: 'info',
        })
        props.onAdd &&
            props.onAdd({
                id: response.id,
                firstName: response.firstName,
                lastName: response.lastName,
                email: response.email,
            })
        handleClose()
    }

    const values = React.useMemo(() => {
        const isEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(props.input!)
        if (isEmail) {
            return {
                email: input,
                name: '',
            }
        } else {
            return {
                name: input,
                email: '',
            }
        }
    }, [])

    const initialValues: CustomerAddInput = {
        ...CustomerAddInitialValues,
        ...values,
        timezone,
        sendEmail: true,
        sendSms: true,
    }

    return (
        <FormContainer
            open={open}
            onClose={handleClose}
            onExited={navigateBack}
            header={<Trans>New {alias?.singular}</Trans>}
            formId={formId}
            loading={loading}
            {...props}
        >
            <CustomerAddFormComponent<CustomerAddInput>
                onSubmit={handleSubmit}
                id={formId}
                add
                initialValues={initialValues}
                validationSchema={CustomerAddValidations}
            />
        </FormContainer>
    )
}

export default CustomerAdd
