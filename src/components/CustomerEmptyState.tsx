import { Trans } from '@lingui/macro'
import React from 'react'
import { EmptyStateContainer } from '@saastack/layouts/containers'
import { useAlias } from '@saastack/core'

interface Props {
    onAction?: () => void
}

const CustomerEmptyState: React.FC<Props> = ({ onAction }) => {
    const alias = useAlias('Customers', { singular: 'Customer', plural: 'Customers' })
    const props = {
        image: `${process.env.REACT_APP_CDN_URL}/images/empty-states/new-customer-empty-state.png`,
        actionLabel: <Trans>Add {alias?.singular}</Trans>,
        message: <Trans>Add your first {alias?.singular.toLocaleLowerCase()}</Trans>,
    }
    return <EmptyStateContainer onAction={onAction} {...props} />
}

export default CustomerEmptyState
