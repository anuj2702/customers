import { Trans } from '@lingui/macro'
import { Mutable, useAlert, useAlias, useConfig } from '@saastack/core'
import { FormContainerProps } from '@saastack/layouts'
import { FormContainer } from '@saastack/layouts/containers'
import { useNavigate, useParams } from '@saastack/router'
import { tail } from 'lodash-es'
import React from 'react'
import { createFragmentContainer } from 'react-relay'
import { useRelayEnvironment } from 'react-relay/hooks'
import { graphql } from 'relay-runtime'
import { CustomerUpdate_customers } from '../__generated__/CustomerUpdate_customers.graphql'
import { CustomerInput } from '../__generated__/UpdateCustomerMutation.graphql'
import CustomerUpdateFormComponent from '../forms/CustomerUpdateFormComponent'
import UpdateCustomerMutation from '../mutations/UpdateCustomerMutation'
import CustomerAddValidations from '../utils/CustomerAddValidations'
import { useQuery } from '@saastack/relay'
import { GetCompanyQuery } from '../__generated__/GetCompanyQuery.graphql'
import query from '../queries/GetCompanyQuery'
import { Loading } from '@saastack/components'

interface Props extends Omit<FormContainerProps, 'formId'> {
    customers: CustomerUpdate_customers
    companyAdmin: boolean
}

const formId = 'customer-update-form'

const CustomerUpdate: React.FC<Props> = ({ customers, companyAdmin, ...props }) => {
    const alias = useAlias('Customers', { singular: 'Customer', plural: 'Customers' })
    const { companyId } = useConfig()
    const environment = useRelayEnvironment()
    const showAlert = useAlert()
    const navigate = useNavigate()
    const [open, setOpen] = React.useState(true)
    const [loading, setLoading] = React.useState(false)
    const { data, loading: dataLoading } = useQuery<GetCompanyQuery>(
        query,
        { id: companyId },
        { fetchPolicy: 'network-only' }
    )

    const { id } = useParams()
    const customer = customers.find((i) => i?.id === window.atob(id!))

    const navigateBack = () => navigate('../')
    const handleClose = () => setOpen(false)
    React.useEffect(() => {
        if (!customer) {
            handleClose()
        }
    }, [customer])
    if (!customer) {
        return null
    }
    if (dataLoading) {
        return <Loading />
    }
    const handleSubmit = (values: CustomerInput) => {
        setLoading(true)
        const nameParts = ((values as any).name || '').trim().split(' ')
        const customer = {
            ...values,
            firstName: nameParts[0],
            lastName: tail(nameParts).join(' '),
        }
        UpdateCustomerMutation.commit(
            environment,
            customer,
            ['firstName', 'companyId', 'lastName', 'email', 'telephones', 'birthDate', 'address'],
            companyAdmin,
            {
                onSuccess,
                onError,
            }
        )
    }

    const initialValues: CustomerInput = ({
        ...customer,
        name: `${customer.firstName} ${customer.lastName}`,
        address: {
            ...customer.address,
            country: Boolean(customer.address?.country?.length)
                ? customer.address?.country
                : data?.company.address?.country,
            region: Boolean(customer.address?.region?.length)
                ? customer.address?.region
                : data?.company.address?.region,
            locality: Boolean(customer.address?.locality?.length)
                ? customer.address?.locality
                : data?.company.address?.locality,
            birthDate: null,
        },
    } as unknown) as Mutable<CustomerInput>

    const onError = (e: string) => {
        setLoading(false)
        showAlert(e, {
            variant: 'error',
        })
    }

    const onSuccess = () => {
        setLoading(false)
        showAlert(<Trans>{alias?.singular} updated successfully!</Trans>, {
            variant: 'info',
        })
        handleClose()
    }

    return (
        <FormContainer
            open={open}
            onClose={handleClose}
            onExited={navigateBack}
            header={<Trans>Update {alias?.singular}</Trans>}
            formId={formId}
            loading={loading}
            {...props}
        >
            <CustomerUpdateFormComponent<CustomerInput>
                onSubmit={handleSubmit}
                id={formId}
                initialValues={initialValues}
                validationSchema={CustomerAddValidations}
            />
        </FormContainer>
    )
}

export default createFragmentContainer(CustomerUpdate, {
    customers: graphql`
        fragment CustomerUpdate_customers on Customer @relay(plural: true) {
            id
            firstName
            lastName
            email
            telephones
            birthDate
            companyId
            address {
                country
                latitude
                longitude
                locality
                postalCode
                region
                streetAddress
            }
        }
    `,
})
