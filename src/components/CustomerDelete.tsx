import { PubSub } from '@saastack/pubsub'
import { Trans } from '@lingui/macro'
import React from 'react'
import { Variables } from 'relay-runtime'
import namespace from '../namespace'
import { useRelayEnvironment } from 'react-relay/hooks'
import { ConfirmContainerProps } from '@saastack/layouts'
import { useAlert, useAlias } from '@saastack/core'
import { ConfirmContainer } from '@saastack/layouts/containers'
import { useNavigate, useParams } from '@saastack/router'
import DeleteCustomerMutation from '../mutations/DeleteCustomerMutation'

interface Props extends Omit<ConfirmContainerProps, 'heading' | 'message' | 'onAction'> {
    variables: Variables
    refetch: () => void
}

const CustomerDelete: React.FC<Props> = ({ variables, refetch, ...props }) => {
    const alias = useAlias('Customers', { singular: 'Customer', plural: 'Customers' })
    const environment = useRelayEnvironment()
    const { id } = useParams()
    const navigate = useNavigate()
    const [open, setOpen] = React.useState(true)
    const showAlert = useAlert()
    const [loading, setLoading] = React.useState(false)

    const handleDelete = () => {
        setLoading(true)
        DeleteCustomerMutation.commit(environment, variables, window.atob(id!), {
            onSuccess,
            onError,
        })
    }

    const navigateBack = () => navigate('../')
    const handleClose = () => setOpen(false)

    const onError = (e: string) => {
        setLoading(false)
        showAlert(e, {
            variant: 'error',
        })
    }

    const onSuccess = (id: string) => {
        PubSub.publish(namespace.delete, id)
        setLoading(false)
        refetch()
        showAlert(<Trans>{alias?.singular} deleted successfully!</Trans>, {
            variant: 'info',
        })
        handleClose()
    }
    return (
        <ConfirmContainer
            loading={loading}
            header={<Trans>Delete {alias?.singular}</Trans>}
            open={open}
            onClose={handleClose}
            onExited={navigateBack}
            onAction={handleDelete}
        />
    )
}

export default CustomerDelete
