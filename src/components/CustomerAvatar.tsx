import loadable from '@loadable/component'
import { Box, IconButton } from '@material-ui/core'
import React from 'react'
import { createFragmentContainer, graphql } from 'react-relay'
import { CustomerAvatar_customer } from '../__generated__/CustomerAvatar_customer.graphql'
import { useRelayEnvironment } from 'react-relay/hooks'
import { UploadResponse } from '@saastack/components'
import UpdateCustomerMutation from '../mutations/UpdateCustomerMutation'
import Avatar from '@saastack/components/Avatar'

const Upload = loadable(() => import('@saastack/components/Upload'))

interface Props {
    customer: CustomerAvatar_customer
    companyAdmin: boolean
}

const CustomerAvatar: React.FC<Props> = ({ customer, companyAdmin }) => {
    const [open, setOpen] = React.useState(false)
    const environment = useRelayEnvironment()

    const handleOpen = (e: React.MouseEvent<HTMLButtonElement>) => {
        e.stopPropagation()
        setOpen(true)
    }
    const handleClose = () => setOpen(false)
    const handleUpload = (profileImage: UploadResponse | UploadResponse[]) => {
        const updateCustomerImageInput = {
            id: customer.id,
            profileImage: profileImage as UploadResponse,
        }
        UpdateCustomerMutation.commit(
            environment,
            updateCustomerImageInput,
            ['profileImage'],
            companyAdmin,
            { onSuccess: handleClose }
        )
    }

    const stopPropagation = (e: React.MouseEvent<HTMLDivElement>) => e.stopPropagation()

    const image = customer.profileImage && customer.profileImage.thumbImage
    return (
        <Box onClick={stopPropagation}>
            {open && (
                <Upload
                    src={image as any}
                    onComplete={handleUpload}
                    open={open}
                    onClose={handleClose}
                />
            )}
            <IconButton size="small" onClick={handleOpen}>
                <Avatar src={customer?.profileImage?.thumbImage} title={customer?.firstName} />
            </IconButton>
        </Box>
    )
}

export default createFragmentContainer(CustomerAvatar, {
    customer: graphql`
        fragment CustomerAvatar_customer on Customer {
            id
            firstName
            profileImage {
                largeImage
                thumbImage
            }
        }
    `,
})
