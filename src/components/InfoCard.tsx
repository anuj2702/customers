import { Card, CardContent, Theme, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import React from 'react'

interface Props {
    heading?: React.ReactNode
}

const useStyles = makeStyles(
    ({
        spacing,
        typography: { body2, h6, fontWeightLight, fontWeightRegular },
        palette: { divider, text },
    }: Theme) => ({
        card: {
            border: `1px solid ${divider}`,
            minWidth: 180,
        },
        cardContent: {
            textAlign: 'center',
            '&, &:first-child, &:last-child': {
                padding: spacing(1.2, 2.4),
            },
        },
        heading: {
            ...body2,
            color: text.secondary,
            fontWeight: fontWeightLight,
            marginTop: -2,
        },
        content: {
            ...h6,
            fontWeight: fontWeightRegular,
        },
    })
)

const InfoCard: React.FC<Props> = ({ heading, children }) => {
    const classes = useStyles()
    return (
        <Card className={classes.card} elevation={0}>
            <CardContent className={classes.cardContent}>
                <Typography className={classes.content}>{children}</Typography>
                {heading && <Typography className={classes.heading}>{heading}</Typography>}
            </CardContent>
        </Card>
    )
}

export default InfoCard
