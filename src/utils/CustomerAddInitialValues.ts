import { CustomerInput } from '../__generated__/CreateCustomerMutation.graphql'

const CustomerAddInitialValues: CustomerInput = {
    firstName: '',
    lastName: '',
    company: '',
    address: {
        country: '',
        latitude: 0,
        longitude: 0,
        locality: '',
        postalCode: '',
        region: '',
        streetAddress: '',
    },
    telephones: [''],
    email: '',
    tag: [],
    birthDate: null,
    preferredLanguage: '',
    timezone: '',
    profileImage: {
        largeImage: '',
        thumbImage: '',
    },
    metadata: 'e30=',
    name: '',
    locationIds: [],
} as any

export default CustomerAddInitialValues
