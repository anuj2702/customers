import { MutationCallbacks } from '@saastack/relay'
import { commitMutation, graphql } from 'react-relay'
import { Disposable, Environment } from 'relay-runtime'
import {
    CustomerPasswordResetInput,
    CustomerPasswordResetMutation,
    CustomerPasswordResetMutationResponse,
} from '../__generated__/CustomerPasswordResetMutation.graphql'

const mutation = graphql`
    mutation CustomerPasswordResetMutation($input: CustomerPasswordResetInput) {
        customerPasswordReset(input: $input) {
            clientMutationId
            payload
        }
    }
`

let tempID = 0

const commit = (
    environment: Environment,
    customerId: string,
    callbacks?: MutationCallbacks<string>
): Disposable => {
    const input: CustomerPasswordResetInput = {
        customerId,
        clientMutationId: `${tempID++}`,
    }
    return commitMutation<CustomerPasswordResetMutation>(environment, {
        mutation,
        variables: {
            input,
        },
        onError: (error: Error) => {
            if (callbacks && callbacks.onError) {
                const message = error.message.split('\n')[1]
                callbacks.onError!(message)
            }
        },
        onCompleted: (response: CustomerPasswordResetMutationResponse) => {
            if (callbacks && callbacks.onSuccess) {
                callbacks.onSuccess(customerId)
            }
        },
    })
}

export default { commit }
