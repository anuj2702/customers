import { MutationCallbacks } from '@saastack/relay'
import { commitMutation, graphql, Variables } from 'react-relay'

import { Disposable, Environment } from 'relay-runtime'
import {
    DeleteCustomerInput,
    DeleteCustomerMutation,
} from '../__generated__/DeleteCustomerMutation.graphql'

const mutation = graphql`
    mutation DeleteCustomerMutation($input: DeleteCustomerInput) {
        deleteCustomer(input: $input) {
            clientMutationId
        }
    }
`

let tempID = 0

const commit = (
    environment: Environment,
    variables: Variables,
    id: string,
    callbacks?: MutationCallbacks<string>
): Disposable => {
    const input: DeleteCustomerInput = {
        id,
        clientMutationId: `${tempID++}`,
    }

    return commitMutation<DeleteCustomerMutation>(environment, {
        mutation,
        variables: {
            input,
        },
        onError: (error: Error) => {
            if (callbacks && callbacks.onError) {
                const message = error.message.split('\n')[1]
                callbacks.onError!(message)
            }
        },
        onCompleted: () => {
            if (callbacks && callbacks.onSuccess) {
                callbacks.onSuccess(id)
            }
        },
    })
}

export default { commit }
