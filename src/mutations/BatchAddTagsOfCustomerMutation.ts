import { commitMutation, graphql } from 'react-relay'
import { Disposable, Environment, RecordSourceSelectorProxy } from 'relay-runtime'
import { BatchAddTagsOfCustomerInput } from '../__generated__/BatchAddTagsOfCustomerMutation.graphql'
import { MutationCallbacks, setNodeValue } from '@saastack/relay'
import { uniq } from 'lodash-es'

const mutation = graphql`
    mutation BatchAddTagsOfCustomerMutation($input: BatchAddTagsOfCustomerInput) {
        batchAddTagsOfCustomer(input: $input) {
            clientMutationId
            payload
        }
    }
`

let tempID = 0
const sharedUpdater = (store: RecordSourceSelectorProxy, ids: string[], tag: string[]) => {
    ids.forEach((id) => {
        const node = store.get(id)
        if (node) {
            setNodeValue(store, node, {
                tag: uniq([...((node.getValue('tag') as string[]) ?? []), ...tag]),
            })
        }
    })
}

const commit = (
    environment: Environment,
    ids: string[],
    tags: string[],
    callbacks?: MutationCallbacks<{}>
): Disposable => {
    const input: BatchAddTagsOfCustomerInput = {
        ids: ids,
        tags: tags,
        clientMutationId: `${tempID++}`,
    }

    return commitMutation(environment, {
        mutation,
        variables: {
            input,
        },
        updater: (store: RecordSourceSelectorProxy) => sharedUpdater(store, ids, tags),
        onError: (error: Error) => {
            if (callbacks && callbacks.onError) {
                const message = error.message.split('\n')[1]
                callbacks.onError!(message)
            }
        },
        onCompleted: () => {
            if (callbacks && callbacks.onSuccess) {
                callbacks.onSuccess({})
            }
        },
    })
}

export default { commit }
