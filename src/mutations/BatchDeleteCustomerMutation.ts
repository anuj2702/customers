import { commitMutation, graphql } from 'react-relay'
import {
    ConnectionHandler,
    Disposable,
    Environment,
    RecordSourceSelectorProxy,
} from 'relay-runtime'
import { BatchDeleteCustomerInput } from '../__generated__/BatchDeleteCustomerMutation.graphql'
import { MutationCallbacks } from '@saastack/relay'

const mutation = graphql`
    mutation BatchDeleteCustomerMutation($input: BatchDeleteCustomerInput) {
        batchDeleteCustomer(input: $input) {
            clientMutationId
            payload
        }
    }
`

let tempID = 0
const sharedUpdater = (store: RecordSourceSelectorProxy, ids: string[]) => {
    const rootProxy = store.getRoot()
    const connection = ConnectionHandler.getConnection(rootProxy, 'CustomerMaster_customers', [])
    if (connection) {
        ids.forEach((id) => ConnectionHandler.deleteNode(connection, id))
    }
}

const commit = (
    environment: Environment,
    ids: string[],
    callbacks?: MutationCallbacks<{}>
): Disposable => {
    const input: BatchDeleteCustomerInput = {
        ids: ids,
        clientMutationId: `${tempID++}`,
    }

    return commitMutation(environment, {
        mutation,
        variables: {
            input,
        },
        updater: (store: RecordSourceSelectorProxy) => sharedUpdater(store, ids),
        onError: (error: Error) => {
            if (callbacks && callbacks.onError) {
                const message = error.message.split('\n')[1]
                callbacks.onError!(message)
            }
        },
        onCompleted: () => {
            if (callbacks && callbacks.onSuccess) {
                callbacks.onSuccess({})
            }
        },
    })
}

export default { commit }
