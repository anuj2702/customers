import { graphql } from 'react-relay'

const GroupedCustomerReportQuery = graphql`
    query GroupedCustomerReportQuery(
        $dateRange: DateslotInput
        $groupByMonth: Boolean
        $limit: Int
        $locationIds: [String]
        $offset: Int
        $parent: String
        $timezone: String
    ) {
        groupedCustomerSingupReport(
            dateRange: $dateRange
            groupByMonth: $groupByMonth
            limit: $limit
            locationIds: $locationIds
            offset: $offset
            parent: $parent
            timezone: $timezone
        ) {
            edges {
                node {
                    signupOn
                    customerCount
                }
                cursor
            }
            pageInfo {
                nextOffset
                hasPreviousPage
                previousOffset
                hasNextPage
            }
        }
    }
`

export default GroupedCustomerReportQuery
