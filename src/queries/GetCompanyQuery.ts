import { graphql } from 'relay-runtime'

const GetCompanyQuery = graphql`
    query GetCompanyQuery($id: ID) {
        company(id: $id) {
            id
            address {
                country
                region
                locality
            }
        }
    }
`

export default GetCompanyQuery
