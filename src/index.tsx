import { PubSub } from '@saastack/pubsub'
import namespace from './namespace'
import React from 'react'

PubSub.register(Object.values(namespace))

let loaded = false

const load = () => {
    if (loaded) {
        return
    }
    loaded = true
}

load()
